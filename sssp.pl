%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           MEMBRI GRUPPO PROGETTO                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%    NOME/COG:  Luca Samuele Vanzo  (735924)                                   %
%    NOME/COG:  Stefano Secci       (756610)                                   %
%    NOME/COG:  Marco Piovani       (735380)                                   %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          INFORMAZIONI DI VERSIONE                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%    VER.FILE:  22                                                             %
%    BASE.REV:  ba9d91bd002df17fc6ce3d85d6dd8242e7e49f8d                       %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      INFORMAZIONI/RICHIESTE PROGETTO                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%    == Introduzione ==                                                        %
%    Calcolare il percorso più breve da un punto a un altro di una mappa       %
%    è un problema più che noto. Vi sono diversi algoritmi in grado di         %
%    risolvere questo problema, noto in letteratura come il “Single Source     %
%    Shortest Path Problem” (SSSP Problem, cfr., [CLR+09] capitolo 24).        %
%    Lo scopo di questo progetto è l’implementazione dell’algoritmo di         %
%    Dijkstra (cfr., [CLR+09] 24.3), che risolve il problema SSSP per          %
%    grafi diretti con distanze tra vertici non negative.                      %
%                                                                              %
%    Per procedere all’implementazione di quest’algoritmo è necessaria – e,    %
%    di fatto, è la parte principale del progetto – produrre una               %
%    implementazione di un MINHEAP (o MINPRIORITYQUEUE).                       %
%                                                                              %
%    Inoltre, possiamo pensare di inserire degli altri dati nella base-dati    %
%    Prolog; ad esempio, possiamo inserire informazioni riguardanti associate  %
%    ad ogni vertice mediante dei predicati che rappresentano queste           %
%    associazioni. Ad esempio, potremmo supporre che ad ogni vertice sia       %
%    associata una posizione su una mappa.                                     %
%                                                                              %
%    Una volta scelta una rappresentazione in memoria di un grafo (diretto)    %
%    è semplice manipolare i grafi in Prolog e costruire delle API che ci      %
%    permettono di costruire algoritmi più complessi, quali l’algoritmo        %
%    di Dijkstra per la soluzione del problema SSSP.                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              OUTPUT FLAG PROLOG                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %
:- set_prolog_flag(                                 % Forzatura output LIST
            toplevel_print_options,                 %             |
            [quoted(true), portray(true)]           %             |
        ).                                          % ------------+
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      PREDICATI DI CONTROLLO E VERIFICA                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   == Inizio Macro-Sezione ==                                                 %
%   La seguente porzione di listato permette la verifica a più step dei        %
%   predicati presenti o meno nel database prolog. Ad esempio potrei           %
%   verificare se nel database esiste almeno un dato predicato foo/X.          %
%   Non solo, passo successivo, posso verificate se esiste un predicato        %
%   foo( ... ) avente [X, Y, Z, ...] parametri.                                %
%                                                                              %
%   Inoltre definisce un valore costante per INFINITE/1.                       %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %
verify(Goal) :-                                     % Inizio:
        current_predicate(Goal), !.                 % Controllo esistenza Goal
                                                    %
verify(Goal, Name, Arguments) :-                    % Inizio:
        current_predicate(Goal), !,                 % Controllo esistenza Goal
        Term =.. [Name | Arguments],                % Creazione argomento CALL
        call(Term).                                 % Verifica di Goal(Term)
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
infinite(X) :- X is 999999999999999999999999999999. % INFINITO
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                PREDICATI PER CREAZIONE E MANIPOLAZIONE GRAFI                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   == Inizio Macro-Sezione ==                                                 %
%   La seguente porzione di listato è composta da cinque predicati:            %
%                                                                              %
%       - NEW_GRAPH/1                                                          %
%       - DELETE_GRAPH/1                                                       %
%       - ADD_VERTEX/2                                                         %
%       - ADD_ARC/3                                                            %
%       - ADD_ARC/4                                                            %
%                                                                              %
%   Grazie ad essi è possibile creare, eliminare e manipolare un grafo di N    %
%   vertici aventi M archi orientati. La differenza principale tra ADD_ARC/3   %
%   ed ADD_ARC/4 consiste nel porre la distanza tra U -> V a 1 nella versione  %
%   del predica con arità 3 (parametri).                                       %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %
new_graph(G) :-                                     % Inizio:
        nonvar(G), !,                               % Controllo se non variabile
        not(                                        % ------------+
            verify(graph/1, graph, [G])             %             |
        ), !,                                       % Negazione esist. Graph(G)
        assert(graph(G)).                           % Creazione goal GRAPH(G)
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Predicato per imozione grafi                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %
delete_graph(G) :-                                  % Inizio:
        nonvar(G), !,                               % Controllo se non variabile
        (                                           % OR >>===================>>
            delete_graph_helper_st(G) ;             %      CALL Helper(G, _)
            true                                    %      TRUE (Forzatura)
        ),                                          % <<===================<< OR
        (                                           % OR >>===================>>
            delete_graph_helper_nd(G) ;             %      CALL Helper(G, _, _)
            true                                    %      TRUE (Forzatura)
        ),                                          % <<===================<< OR
        retract(graph(G)), !.                       % Rimozione GRAPH(G) da DB
                                                    %
delete_graph_helper_st(G) :-                        % Inizio:
        retract(arc(G, _, _, _)),                   % Rimozione ARC(G, _, _, _)
        delete_graph_helper_st(G).                  % Ricorsione: >>==HELPER==>>
                                                    %
delete_graph_helper_nd(G) :-                        % Inizio:
        retract(vertex(G, _)),                      % Rimozione VERTEX(G, _).
        delete_graph_helper_nd(G).                  % Ricorsione: >>==HELPER==>>
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Predicati per aggiunta vertici ed archi                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %
add_vertex(G, V) :-                                 % Inizio:
        nonvar(G), !,                               % Controllo se non variabile
        nonvar(V), !,                               % Controllo se non variabile
        verify(graph/1, graph, [G]), !,             % Controllo esist. Graph(G)
        not(                                        % ------------+
            verify(vertex/2, vertex, [G, V])        %             |
        ), !,                                       % Negazione esist. Vertex(V)
        assert(vertex(G, V)).                       % Creazione di VERTEX(V) > G
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
add_arc(G, U, V) :-                                 % Inizio:
        add_arc(G, U, V, 1).                        % Creo ARC con peso 1
                                                    %
add_arc(G, U, V, Weight) :-                         % Inizio:
        nonvar(G), !,                               % Controllo se non variabile
        nonvar(U), !,                               % Controllo se non variabile
        nonvar(V), !,                               % Controllo se non variabile
        nonvar(Weight), !,                          % Controllo se non variabile
        verify(graph/1, graph, [G]), !,             % Controllo esist. Graph(G)
        verify(vertex/2), !,                        % Controllo esist. Vertex(V)
        vertex(G, U), !,                            % Verifico vertice U
        vertex(G, V), !,                            % Verifico vertice V
        not(                                        % ------------+
            verify(arc/4, arc, [G, U, V, _])        %             |
        ), !,                                       % Negazione esist. ARC(U, V)
        assert(arc(G, U, V, Weight)).               % Creazione goal ARC(U,V,Z)
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      PREDICATI DI CONVERSIONE IN LISTA                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   == Inizio Macro-Sezione ==                                                 %
%   La seguente porzione di listato permette la conversione in lista di più    %
%   predicati aventi lo stesso pattern di ricerca: foo(X) => [foo(X), ...]     %
%                                                                              %
%   Inoltre permette di ottenere in forma di lista tutti gli archi,            %
%   dunque arc(G, V, U, Weight), aventi origine nel vertice V.                 %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %
vertices(G, Vs) :-                                  % Inizio:
        verify(graph/1, graph, [G]),                % Controllo esist. Graph(G)
        verify(vertex/2),                           % Controllo esist. vertici
        findall(vertex(G, X),                       % Pattern di Conversione
                vertex(G, X),                       % Predicato di ricerca
                Vs),                                % Output in forma Lista []
        not(Vs == []).                              % Controllo su Output != []
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
arcs(G, Es) :-                                      % Inizio:
        verify(graph/1, graph, [G]),                % Controllo esist. Graph(G)
        verify(arc/4),                              % Controllo esist. archi
        findall(arc(G, X, Y, Z),                    % Pattern di Conversione
                arc(G, X, Y, Z),                    % Predicato di ricerca
                Es),                                % Output in forma Lista []
        not(Es == []).                              % Controllo su Output != []
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
neighbors(G, V, Ns) :-                              % Inizio:
        nonvar(G), !,                               % Controllo se non variabile
        nonvar(V), !,                               % Controllo se non variabile
        verify(graph/1, graph, [G]), !,             % Controllo esist. Graph(G)
        verify(vertex/2), !,                        % Controllo esist. vertici
        verify(arc/4), !,                           % Controllo esist. archi
        findall(arc(G, V, N, W),                    % Pattern di Conversione
                arc(G, V, N, W),                    % Predicato di ricerca
                Ns),                                % Output in forma Lista []
        not(Ns == []).                              % Controllo su Output != []
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                PREDICATI DI OUTPUT DIJKSTRA => SHORTEST-PATH                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   == Inizio Macro-Sezione ==                                                 %
%   La seguente porzione di listato permette la creazione e manipolazione      %
%   dei predicati generabili da SSSP, ovvero algoritmo di Dijkstra, per        %
%   poi poter calcolare il cammino minimo da Source a un vertice Dest.         %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %
add_dist(G, V, Z) :-                                % Inizio:
        nonvar(G),                                  % Controllo se non variabile
        nonvar(V),                                  % Controllo se non variabile
        nonvar(Z),                                  % Controllo se non variabile
        not(                                        % ------------+
            verify(dist/3, dist, [G, V, _])         %             |
        ),                                          % Negazione esist. DIST
        verify(graph/1, graph, [G]), !,             % Controllo esist. Graph(G)
        verify(vertex/2, vertex, [G, V]), !,        % Controllo esist. Vertex(V)
        assert(dist(G, V, Z)).                      % Creazione goal DIST
                                                    %
add_dist(G, V, Z) :-                                % Inizio:
        nonvar(G),                                  % Controllo se non variabile
        nonvar(V),                                  % Controllo se non variabile
        nonvar(Z),                                  % Controllo se non variabile
        verify(dist/3, dist, [G, V, _]),            % Controllo esist. DIST
        verify(graph/1, graph, [G]), !,             % Controllo esist. Graph(G)
        verify(vertex/2, vertex, [G, V]), !,        % Controllo esist. Vertex(V)
        change_dist(G, V, Z).                       % Creazione goal DIST
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
add_visited(G, V) :-                                % Inizio:
        nonvar(G), !,                               % Controllo se non variabile
        nonvar(V), !,                               % Controllo se non variabile
        verify(graph/1, graph, [G]), !,             % Controllo esist. Graph(G)
        verify(vertex/2, vertex, [G, V]), !,        % Controllo esist. Vertex(V)
        not(                                        % ------------+
            verify(visited/2, visited, [G, V])      %             |
        ), !,                                       % Controllo goal duplicati
        assert(visited(G, V)).                      % Creazione goal DIST
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
add_previous(G, V, U) :-                            % Inizio:
        nonvar(G),                                  % Controllo se non variabile
        nonvar(V),                                  % Controllo se non variabile
        nonvar(U),                                  % Controllo se non variabile
        not(                                        % ------------+
            verify(previous/3, previous, [G, V, _]) %             |
        ),                                          % Negazione esist. PREVIOUS
        verify(graph/1, graph, [G]), !,             % Controllo esist. Graph(G)
        verify(vertex/2, vertex, [G, V]), !,        % Controllo esist. Vertex(V)
        verify(vertex/2, vertex, [G, U]), !,        % Controllo esist. Vertex(U)
        assert(previous(G, V, U)).                  % Creazione goal PREVIOUS
                                                    %
add_previous(G, V, U) :-                            % Inizio:
        nonvar(G),                                  % Controllo se non variabile
        nonvar(V),                                  % Controllo se non variabile
        nonvar(U),                                  % Controllo se non variabile
        verify(previous/3, previous, [G, V, _]),    % Controllo esist. PREVIOUS
        verify(graph/1, graph, [G]), !,             % Controllo esist. Graph(G)
        verify(vertex/2, vertex, [G, V]), !,        % Controllo esist. Vertex(V)
        verify(vertex/2, vertex, [G, U]), !,        % Controllo esist. Vertex(U)
        change_previous(G, V, U).                   % Creazione goal PREVIOUS
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
change_dist(G, V, NewDist) :-                       % Inizio:
        nonvar(G), !,                               % Controllo se non variabile
        nonvar(V), !,                               % Controllo se non variabile
        nonvar(NewDist), !,                         % Controllo se non variabile
        verify(graph/1, graph, [G]), !,             % Controllo esist. Graph(G)
        verify(vertex/2, vertex, [G, V]), !,        % Controllo esist. Vertex(V)
        (                                           % OR >>===================>>
            change_dist_helper(G, V, _) ;           %      CALL Helper(G, V, _)
            true                                    %      TRUE (Forzatura)
        ),                                          % <<===================<< OR
        assert(dist(G, V, NewDist)), !.             % Creazione goal DIST
                                                    %
change_dist_helper(G, V, _) :-                      % Inizio:
        retract(dist(G, V, _)),                     % Rimozione DIST(G,V,_)
        change_dist_helper(G, V, _).                % Ricorsione: >>==HELPER==>>
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
change_previous(G, V, U) :-                         % Inizio:
        nonvar(G), !,                               % Controllo se non variabile
        nonvar(V), !,                               % Controllo se non variabile
        nonvar(U), !,                               % Controllo se non variabile
        verify(graph/1, graph, [G]), !,             % Controllo esist. Graph(G)
        verify(vertex/2, vertex, [G, V]), !,        % Controllo esist. Vertex(V)
        verify(vertex/2, vertex, [G, U]), !,        % Controllo esist. Vertex(U)
        (                                           % OR >>===================>>
            change_previous_helper(G, V, _) ;       %      CALL Helper(G, V, _)
            true                                    %      TRUE (Forzatura)
        ),                                          % <<===================<< OR
        assert(previous(G, V, U)), !.               % Creazione goal PREVIOUS
                                                    %
change_previous_helper(G, V, _) :-                  % Inizio:
        retract(previous(G, V, _)),                 % Rimozione PREVIOUS(G,V,_)
        change_previous_helper(G, V, _).            % Ricorsione: >>==HELPER==>>
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                PREDICATI PER CREAZIONE E MANIPOLAZIONE HEAP                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   == Inizio Macro-Sezione ==                                                 %
%   La seguente porzione di listato è composta da otto predicati:              %
%                                                                              %
%       - NEW_HEAP/1                                                           %
%       - HEAP_SIZE/2                                                          %
%       - EMPTY/1                                                              %
%       - NOT_EMPTY/1                                                          %
%       - INSERT/3                                                             %
%       - EXTRACT/3                                                            %
%       - HEAD/3                                                               %
%       - MODIFY_KEY/4                                                         %
%                                                                              %
%   La MinHeap-Property è mantenuta applicando l'algoritmo Heapify su          %
%   predicato EXTRACT/3 dalla root, applicando DecreaseKey su predicato        %
%   INSERT/3 da Size e, infine, applicando DecreaseKey (se nuova chiave è      %
%   minore della precedente) oppure Heapify (se nuova chiave è maggiore        %
%   della precedente) partendo dal nodo di lavoro.                             %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %
new_heap(H) :-                                      % Inizio:
        nonvar(H), !,                               % Controllo se non variabile
        not(                                        % ------------+
            verify(heap/2, heap, [H, _])            %             |
        ), !,                                       % Negazione esist. HEAP(H,_)
        assert(heap(H, 0)), !.                      % Creazione gloa HEAP(H, 0)
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
heap_size(H, S) :-                                  % Inizio:
        nonvar(H), !,                               % Controllo se non variabile
        verify(heap/2, heap, [H, _]), !,            % Controllo esist. HEAP(H,_)
        heap(H, S).                                 % Estrazione dimensione di H
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
empty(H) :-                                         % Inizio:
        nonvar(H), !,                               % Controllo se non variabile
        verify(heap/2, heap, [H, _]), !,            % Controllo esist. HEAP(H,_)
        heap(H, S),                                 % Estrazione dimensione di H
        S @=< 0.                                    % Verifica dimensione == 0
                                                    %
not_empty(H) :-                                     % Inizio:
        nonvar(H), !,                               % Controllo se non variabile
        verify(heap/2, heap, [H, _]), !,            % Controllo esist. HEAP(H,_)
        heap(H, S),                                 % Estrazione dimensione di H
        S @> 0.                                     % Verifica dimensione > 0
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
insert(H, K, V) :-                                  % Inizio:
        nonvar(H), !,                               % Controllo se non variabile
        nonvar(K), !,                               % Controllo se non variabile
        nonvar(V), !,                               % Controllo se non variabile
        verify(vertex/2, vertex, [H, V]), !,        % Controllo esist. Vertex(V)
        K @>= 0, !,                                 % Verifico che KEY >= ZERO
        not(                                        % ------------+
            verify(heap_entry/4,                    %             |
                   heap_entry,                      %             |
                   [H, _, _, V])                    %             |
        ), !,                                       % Negazione esist. HEAP(H,_)
        heap(H, Size),                              % Ottengo la dimensione
        Pos is Size + 1,                            % Incremento di 1 e creo Pos
        assert(heap_entry(H, Pos, K, V)),           % Creazione gloa HEAP(H, 0)
        retract(heap(H, Size)),                     % Elimino vecchio HEAP(H)
        assert(heap(H, Pos)),                       % Creazione goal HEAP(H, S)
        heapify_decrese_key(H, Pos), !.             % Riordino Min-Heap
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
extract(H, K, V) :-                                 % Inizio:
        nonvar(H),                                  % Controllo se non variabile
        heap(H, Size),                              % Ottengo dimensione HEAP
        Size @> 1,                                  % Verifico se > di uno
        verify(heap_entry/4), !,                    % Controllo esist. ENTRY
        heap_entry(H, 1, K, V),                     % Verifico associazioni
        retract(heap_entry(H, 1, K, V)),            % Elimino primo ENTRY
        heap_entry(H, Size, SKey, SVal),            % Ottengo dati ultima ENTRY
        retract(heap_entry(H, Size, SKey, SVal)),   % Elimino ultima ENTRY
        assert(heap_entry(H, 1, SKey, SVal)),       % Aggiungo ultima come prima
        Pos is Size - 1,                            % Decremento di 1 e creo Pos
        retract(heap(H, Size)),                     % Elimino vecchio HEAP(H)
        assert(heap(H, Pos)),                       % Creazione goal HEAP(H, S)
        heapify(H), !.                              % Riordino Min-Heap
                                                    %
extract(H, K, V) :-                                 % Inizio:
        nonvar(H),                                  % Controllo se non variabile
        heap(H, Size),                              % Ottengo dimensione HEAP
        Size @=< 1,                                 % Verifico se >= a uno
        verify(heap_entry/4), !,                    % Controllo esist. ENTRY
        heap_entry(H, 1, K, V),                     % Verifico associazioni
        retract(heap_entry(H, 1, K, V)),            % Elimino primo ENTRY
        Pos is Size - 1,                            % Decremento di 1 e creo Pos
        retract(heap(H, Size)),                     % Elimino vecchio HEAP(H)
        assert(heap(H, Pos)),                       % Creazione goal HEAP(H, S)
        heapify(H), !.                              % Riordino Min-Heap
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
head(H, K, V) :-                                    % Inizio:
        nonvar(H), !,                               % Controllo se non variabile
        verify(heap_entry/4), !,                    % Controllo esist. ENTRY
        heap_entry(H, 1, K, V), !.                  % Verifico associazioni
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
modify_key(H, NewKey, OldKey, Value) :-             % Inizio:
        nonvar(H), !,                               % Controllo se non variabile
        NewKey @>= 0, !,                            % Verifico che KEY >= ZERO
        verify(heap_entry/4), !,                    % Controllo esist. ENTRY
        heap_entry(H, Pos, OldKey, Value),          % Verifico associazioni
        retract(heap_entry(H, Pos, OldKey, Value)), % Elimino primo ENTRY
        assert(heap_entry(H, Pos, NewKey, Value)),  % Aggiungo con nuova KEY
        modify_key_helper(H, OldKey, NewKey, Pos).  % Mantenimento HEAP
                                                    %
modify_key_helper(_, OK, NK, _) :-                  % Inizio:
        NK == OK.                                   % Se chiavi equivalenti
                                                    %
modify_key_helper(H, OK, NK, Pos) :-                % Inizio:
        NK @< OK,                                   % Se nuova chiave < vecchia
        heapify_decrese_key(H, Pos).                % Riordino verso la ROOT
                                                    %
modify_key_helper(H, OK, NK, Pos) :-                % Inizio:
        NK @>= OK,                                  % Se nuova chiave > vecchia
        heapify_helper(H, Pos).                     % Riordino verso le foglie
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Predicato per la rimozione heap                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %
delete_heap(H) :-                                   % Inizio:
        nonvar(H), !,                               % Controllo se non variabile
        verify(heap/2, heap, [H, _]), !,            % Controllo esist. HEAP(H,_)
        (                                           % OR >>===================>>
            delete_heap_helper(H) ;                 %      CALL Helper(H)
            true                                    %      TRUE (Forzatura)
        ),                                          % <<===================<< OR
        retract(heap(H, _)), !.                     % Rimozione HEAP(H, _)
                                                    %
delete_heap_helper(H) :-                            % Inizio:
        retract(heap_entry(H, _, _, _)),            % Rimozione ENTRY(H,_,_,_)
        delete_heap_helper(H).                      % Ricorsione: >>==HELPER==>>
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 PREDICATI DI MANTENIMENTO MINHEAP-PROPERTY                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   == Inizio Macro-Sezione ==                                                 %
%   La seguente porzione di listato permette, mediante algoritmi Heapify e     %
%   DecreaseKey, di mantenere inalterate post extract, insert o modify_key     %
%   le proprietà del MinHeap: P0 -> chiave minima, PSize -> chiave max.        %
%                                                                              %
%                                                                              %
%                                   MINHEAP                                    %
%                                 [ 1, R, 4 ]                                  %
%                                  /       \                                   %
%                                 /         \                                  %
%                        [ 2, F, 6 ]       [ 3, F, 10 ]                        %
%                         /      |           |       \                         %
%                        /       |           |        \                        %
%               [ 4, F, 8 ] [ 5, F, 9 ] [ 6, F, 15 ] [ 7, F, 18 ]              %
%                                                                              %
%                                   VETTORE                                    %
%                           [ 1, 2, 3, 4, 5, 6, 7 ]                            %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %
heapify(H) :-                                       % Inizio:
        nonvar(H),                                  % Controllo se non variabile
        verify(heap/2, heap, [H, _]),               % Controllo esist. HEAP
        heap(H, Size),                              % Ottengo dimensione HEAP
        Size @> 1,                                  % Verifico se > di uno
        heapify_helper(H, 1).                       % Avvio algoritmo BUILD-HEAP
                                                    %
heapify(H) :-                                       % Inizio:
        nonvar(H),                                  % Controllo se non variabile
        heap(H, Size),                              % Ottengo dimensione HEAP
        Size @=< 1.                                 % Verifico se <= di uno
                                                    %
heapify_helper(H, Pos) :-                           % Inizio:
        heap(H, Size),                              % Ottengo dimensione HEAP
        Size @> 1,                                  % Verifico se > di uno
        L is 2 * Pos,                               % Creo ramo SX con 2*POS
        R is (2 * Pos) + 1,                         % Creo ramo DX con 2*POS + 1
        heapify_if_min_then_else(H, L, R, Pos).     % Cerco associazione blocco
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
heapify_if_min_then_else(H, L, R, _) :-             % Inizio:
        heap(H, Size),                              % Ottengo dimensione HEAP
        L @> Size,                                  % Caso base ramo SX > SIZE
        R @> Size.                                  % Caso base ramo DX > SIZE
                                                    %
heapify_if_min_then_else(H, L, R, C) :-             % Inizio:
        heap(H, Size),                              % Ottengo dimensione HEAP
        L @=< Size,                                 % Verifico ramo SX <= SIZE
        R @=< Size,                                 % Verifico ramo DX <= SIZE
        heap_entry(H, C, CKey, CVal),               % Ottengo dati ramo ROOT
        heap_entry(H, L, LKey, LVal),               % Ottengo dati ramo LEFT
        LKey @< CKey,                               % Verifico se ramo SX < RT
        retract(heap_entry(H, C, CKey, CVal)),      % ------------+
        retract(heap_entry(H, L, LKey, LVal)),      %  SOSTITUZIONE ROOT-CHILD
        assert(heap_entry(H, L, CKey, CVal)),       %             |
        assert(heap_entry(H, C, LKey, LVal)),       % ------------+
        heapify_helper(H, L).                       % Ricorsione: >>==HELPER==>>
                                                    %
heapify_if_min_then_else(H, L, R, C) :-             % Inizio:
        heap(H, Size),                              % Ottengo dimensione HEAP
        L @=< Size,                                 % Verifico ramo SX <= SIZE
        R @=< Size,                                 % Verifico ramo DX <= SIZE
        heap_entry(H, C, CKey, CVal),               % Ottengo dati ramo ROOT
        heap_entry(H, R, RKey, RVal),               % Ottengo dati ramo RIGHT
        RKey @< CKey,                               % Verifico se ramo DX < RT
        retract(heap_entry(H, C, CKey, CVal)),      % ------------+
        retract(heap_entry(H, R, RKey, RVal)),      %  SOSTITUZIONE ROOT-CHILD
        assert(heap_entry(H, R, CKey, CVal)),       %             |
        assert(heap_entry(H, C, RKey, RVal)),       % ------------+
        heapify_helper(H, R).                       % Ricorsione: >>==HELPER==>>
                                                    %
heapify_if_min_then_else(H, L, R, C) :-             % Inizio:
        heap(H, Size),                              % Ottengo dimensione HEAP
        L @=< Size,                                 % Verifico ramo SX <= SIZE
        R @> Size,                                  % Caso base ramo DX > SIZE
        heap_entry(H, C, CKey, CVal),               % Ottengo dati ramo ROOT
        heap_entry(H, L, LKey, LVal),               % Ottengo dati ramo LEFT
        LKey @< CKey,                               % Verifico se ramo SX < DX
        retract(heap_entry(H, C, CKey, CVal)),      % ------------+
        retract(heap_entry(H, L, LKey, LVal)),      %  SOSTITUZIONE ROOT-CHILD
        assert(heap_entry(H, L, CKey, CVal)),       %             |
        assert(heap_entry(H, C, LKey, LVal)),       % ------------+
        heapify_helper(H, L).                       % Ricorsione: >>==HELPER==>>
                                                    %
heapify_if_min_then_else(_, _, _, _) :- true.       % Forzatura se ordinato
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
heapify_decrese_key(_, C) :-                        % Inizio:
        C @=< 1.                                    % Verifico posizione
                                                    %
heapify_decrese_key(H, C) :-                        % Inizio:
        C @> 1,                                     % Verifico posizione
        heap_entry(H, C, CKey, CVal),               % Ottengo dati ramo corrente
        P is floor(C / 2),                          %
        (                                           %
            heap_entry(H, P, PKey, PVal),           % Ottengo dati ramo corrente
            CKey @< PKey,                           % Verifico se ramo C < P
            retract(heap_entry(H, C, CKey, CVal)),  % ------------+
            retract(heap_entry(H, P, PKey, PVal)),  %  SOSTITUZIONE ROOT-CHILD
            assert(heap_entry(H, P, CKey, CVal)),   %             |
            assert(heap_entry(H, C, PKey, PVal)),   %             |
            heapify_decrese_key(H, P) ;             % ------------+
            true                                    %
        ).                                          %
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  PREDICATI DI RISOLUZIONE SSSP CON DIJKSTRA                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   == Inizio Macro-Sezione ==                                                 %
%   La seguente porzione di listato permette di calcolare il cammino minimo    %
%   da un vertice Source a un vertice Dest partendo da un grafo G avente       %
%   tutti i vertici con chiave in MinHeap iniziale pari a INFINITE/1.          %
%                                                                              %
%   Inoltre grazie al rilassamento è possibile collassare cammini              %
%   alternativi non minimi rispetto ad altri scoperti durante esecuzione.      %
%                                                                              %
%                                                                              %
%   [ 1, S, 4 ]                                [ 5, R, 17 ] --- [ 6, E, 23 ]   %
%            \                                  /                              %
%           [ 2, R, 6 ] --- [ 3, R, 10 ]       /                               %
%                                     \       /          SSSP - DIJKSTRA       %
%                                    [ 4, R, 11 ]                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %
sssp(G, Source) :-                                  % Inizio:
        nonvar(G), !,                               % Controllo se non variabile
        nonvar(Source), !,                          % Controllo se non variabile
        verify(vertex/2), !,                        % Controllo esist. ARCS
        (                                           % OR >>===================>>
            retractall(previous(_, _, _)) ;         %       Rimuovo i PREVIOUS
            true                                    %       TRUE (Forzatura)
        ),                                          % <<===================<< OR
        (                                           % OR >>===================>>
            retractall(visited(_, _)) ;             %       Rimuovo i VISITED
            true                                    %       TRUE (Forzatura)
        ),                                          % <<===================<< OR
        (                                           % OR >>===================>>
            retractall(dist(_, _, _)) ;             %       Rimuovo i DIST
            true                                    %       TRUE (Forzatura)
        ),                                          % <<===================<< OR
        (                                           % OR >>===================>>
            delete_heap(G) ;                        %       Rimuovo HEAP + ENTRY
            true                                    %       TRUE (Forzatura)
        ),                                          % <<===================<< OR
        findall(V,                                  % ------------+
                vertex(G, V),                       %             |
                LVertex),                           % Ottengo lista vertici
        new_heap(G),                                % Creo nuovo HEAP per SSSP
        sssp_init(G, G, LVertex),                   % Inizializzo vertici a INF
        infinite(Inf),                              % Ottengo valore INFINITE
        modify_key(G, 0, Inf, Source),              % Imposto la sorgente a ZERO
        add_dist(G, Source, 0),                     % Creo la DIST per sorgente
        add_previous(G, Source, Source),            % Creo PREVIOUS per sorgente
        dijkstra(G, G), !.                          % CALL algoritmo Dijkstra
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
sssp_init(_, _, []) :- true, !.                     % Inizio: Caso base
                                                    %
sssp_init(G, H, [V | T]) :-                         % Inizio:
        infinite(Inf),                              % Ottengo valore ideale INF.
        insert(H, Inf, V),                          % Inserisco nello HEAP => V
        sssp_init(G, H, T).                         % Ricorsione: >>==HELPER==>>
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
dijkstra(_, H) :-                                   % Inizio:
        heap(H, Size),                              % Ottengo dimensione HEAP
        Size @=< 0,                                 % Controllo se VUOTO
        true, !.                                    % Caso base, TRUE
                                                    %
dijkstra(G, H) :-                                   % Inizio:
        heap(H, Size),                              % Ottengo dimensione HEAP
        Size @> 0,                                  % Controllo se POPOLATO
        extract(H, Key, Value),                     % Ottengo Value con K Minima
        dijkstra_prepare_relax(G, H, Key, Value),   % CALL Rilassamento-Archi
        dijkstra(G, H).                             % Ricorsione: >>==HELPER==>>
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
dijkstra_prepare_relax(G, H, Dist, V) :-            % Inizio:
        verify(arc/4), !,                           % Controllo esist. VISITED
        neighbors(G, V, LArc),                      % Ottengo lista archi da V
        add_visited(G, V),                          % Imposto vertice V visitato
        dijkstra_relax(H, Dist, LArc).              % CALL Ricorsione Dijkstra
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
dijkstra_relax(_, _, []) :-                         % Inizio:
        true, !.                                    % Caso base, TRUE
                                                    %
dijkstra_relax(H, Dist, [arc(G, _, U, _) | T]) :-   % Inizio:
        verify(visited/2, visited, [G, U]),         % Controllo esist. VISITED
        dijkstra_relax(H, Dist, T).                 % Ricorsione: >>==HELPER==>>
                                                    %
dijkstra_relax(H, Dist, [arc(G, V, U, D) | T]) :-   % Inizio:
        not(                                        % ------------+
            verify(visited/2, visited, [G, U])      %             |
        ),                                          % Negazione esist. VISITED
        (                                           % OR >>===================>>
           (heap_entry(H, _, Key, U),               %      Ottengo dati da ENTRY
            Length is Dist + D,                     %      Calcolo Length Dist+D
            Key @> Length,                          %      Controllo se Length <
            add_previous(G, U, V),                  %      Creo PREVIOUS(G,U,V)
            add_dist(G, U, Length),                 %      Creo DIST(G,U,Length)
            modify_key(H, Length, _, U)) ;          %      Modifico KEY per U
            true                                    %      TRUE (Forzatura)
        ),                                          % <<===================<< OR
        dijkstra_relax(H, Dist, T).                 % Ricorsione: >>==HELPER==>>
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              PREDICATI DI RISOLUZIONE SHORTEST-PATH DA DIJKSTRA              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   == Inizio Macro-Sezione ==                                                 %
%   La seguente porzione di listato permette di risolvere un cammino minimo    %
%   da un vertice Source a un vertice Dest partendo dai predicati generati     %
%   precedentemente mediante algoritmo di Dijkstra in SSSP/2.                  %
%                                                                              %
%                                                                              %
%   [ 1, S, 4 ]                                [ 5, R, 17 ] --- [ 6, E, 23 ]   %
%            \                                  /                              %
%           [ 2, R, 6 ] --- [ 3, R, 10 ]       /                               %
%                                     \       /          SSSP - DIJKSTRA       %
%                                    [ 4, R, 11 ]                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %
shortest_path(G, Source, V, Path) :-                % Inizio:
        nonvar(G), !,                               % Controllo se non variabile
        nonvar(Source), !,                          % Controllo se non variabile
        nonvar(V), !,                               % Controllo se non variabile
        verify(dist/3), !,                          % Controllo esist. DIST
        verify(previous/3), !,                      % Controllo esist. PREVIOUS
        dist(G, CheckSource, 0),                    % Ottengo vertice di peso 0
        CheckSource == Source,                      % Controllo se Source = V(0)
        verify(vertex/2, vertex, [G, Source]), !,   % Controllo esist. VERTEX S
        verify(vertex/2, vertex, [G, V]), !,        % Controllo esist. VERTEX V
        shortest_finder(G, Source, Source, V),      % CALL cerco archi da DEST
        findall(arc(G, N, B, D),                    % ------------+
                arc_found(G, N, B, D),              %             |
                LPath),                             % Ottengo lista archi
        retractall(arc_found(G, _, _, _)),          % Rimuovo tutti i temporanei
        shortest_reverser(LPath, Path),             % Inverto la lista PATH
        not(Path == []), !.                         % Verifico se NON vuota!
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
shortest_finder(G, S, _, Pos) :-                    % Inizio:
        Pos == S,                                   % Verifico se POS e S ==
        verify(arc_found/4,                         % Controllo esist. ARC_FOUND
               arc_found,                           %             |
               [G, _, _, _]), !.                    % ------------+
                                                    %
shortest_finder(G, S, _, Pos) :-                    % Inizio:
        Pos == S,                                   % Verifico se POS e S ==
        not(                                        % ------------+
            verify(arc_found/4,                     %             |
                   arc_found,                       %             |
                   [G, _, _, _])                    %             |
        ),                                          % Negazione esist. ARC_FOUND
        assert(arc_found(G, S, Pos, 0)), !.         % Creo ARC_FOUND con D=ZERO
                                                    %
shortest_finder(G, S, Source, Pos) :-               % Inizio:
        not(Pos == S),                              % Verifico se POS e S !=
        not(                                        % ------------+
            verify(previous/3,                      %             |
                   previous,                        %             |
                   [G, Pos, Back])                  %             |
        ),                                          % Negazione esist. PREVIOUS
        not(                                        % ------------+
            verify(arc/4,                           %             |
                   arc,                             %             |
                   [G, Back, Pos, _])               %             |
        ),                                          % Negazione esist. ARC
        assert(arc_found(G, Source, Pos, infinite)).% Creo ARC_FOUND con D=INF.
                                                    %
shortest_finder(G, S, Source, Pos) :-               % Inizio:
        not(Pos == S),                              % Verifico se POS e S !=
        verify(previous/3, previous, [G, Pos, _]),  % Negazione esist. PREVIOUS
        verify(arc/4, arc, [G, _, Pos, _]),         % Negazione esist. ARC
        previous(G, Pos, Back),                     % Controllo esist. PREVIOUS
        arc(G, Back, Pos, Length),                  % Controllo esist. ARC
        assert(arc_found(G, Back, Pos, Length)),    % Creo ARC_FOUND con D=LEN.
        shortest_finder(G, S, Source, Back).        % Ricorsione: >>==HELPER==>>
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% =========================
                                                    %
shortest_reverser(L, A) :-                          % Inizio:
        shortest_reverser(L, [], A).                % CALL Helper per Reverser
                                                    %
shortest_reverser([], A, A):- !.                    % Inizio: Caso base
                                                    %
shortest_reverser([H | T], A, R) :-                 % Inizio:
        shortest_reverser(T, [H | A], R).           % Ricorsione: >>==HELPER==>>
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    PREDICATI DI OUTPUT IN FORMA LISTING                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%   == Inizio Macro-Sezione ==                                                 %
%   La seguente porzione di listato permette di eseguire il LISTING in         %
%   console prolog di vertici, archi, grafi e predicati Dijkstra (debug).      %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %
list_vertices(G) :-                                 % Inizio:
        verify(vertex/2), !,                        % Controllo esist. vertici
        listing(vertex(G, _)), !.                   % Stampo in console: Vertici
                                                    %
list_arcs(G) :-                                     % Inizio:
        verify(arc/4), !,                           % Controllo esist. archi
        listing(arc(G, _, _, _)), !.                % Stampo in console: Archi
                                                    %
list_graph(G) :-                                    % Inizio:
        verify(graph/1),                            % Controllo esist. grafo
        (                                           % OR >>===================>>
            list_vertices(G) ;                      %       DELEGATE
            true                                    %       TRUE (Forzatura)
        ),                                          % <<===================<< OR
        (                                           % OR >>===================>>
            list_arcs(G) ;                          %       DELEGATE
            true                                    %       TRUE (Forzatura)
        ), !.                                       % <<===================<< OR
                                                    %
list_heap(H) :-                                     % Inizio:
        verify(heap/2, heap, [H, _]), !,            % Controllo esist. heap
        verify(heap_entry/4), !,                    % Controllo esist. head
        listing(heap_entry(H, _, _, _)), !.         % Stampo in console: Head
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %
list_dijkstra(G) :-                                 % Inizio:
        verify(visited/2),                          % Controllo esistenza
        verify(previous/3),                         % Controllo esistenza
        verify(dist/3),                             % Controllo esistenza
        listing(visited(G, _)),                     % Stampo in console: Visited
        listing(previous(G, _, _)),                 % Stampo in console: Previo.
        listing(dist(G, _, _)), !.                  % Stampo in console: Dist.
                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              END OF FILE - EOF                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
